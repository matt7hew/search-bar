const inputBar = document.querySelector('.search');
const li = document.querySelectorAll('li');

const containsLetters = (e) => {
	const text = e.target.value.toLowerCase();

	li.forEach((el) => {
		const task = el.textContent;

		if (task.toLowerCase().indexOf(text) !== -1) {
			el.style.display = 'block';
		} else {
			el.style.display = 'none';
		}
	});
};

// const checkKey = (e) => {
// 	if (e.key === "Enter") {
// 		containsLetters();
// 	} else {
// 		console.log('object');
// 	}
// };

inputBar.addEventListener('keyup', containsLetters);
